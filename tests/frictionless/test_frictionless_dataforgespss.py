from dataforge.frictionless.categoricals import table_encode
from frictionless import transform
from frictionless import Package
from pathlib import Path
import pyreadstat
import pandas as pd

FRICTIONLESSDATA_DIR = (
    "https://raw.githubusercontent.com/frictionlessdata/framework/main/data"
)


def test_spss_parser_write_with_missingvalues():

    datapackage = Path("data/countries-with-missing/data-package.json").resolve()
    resource = Package(datapackage).get_resource("spss-csv")
    resource.write(datapackage.parent / "data" / "spss.sav")

    #apply types but not missing values (TODO: probably a way to do via frictionless)
    resource_df = resource.to_petl().todf().apply(pd.to_numeric)

    # read in the newly written sav file 
    df_output, meta_output = pyreadstat.read_sav(
        datapackage.parent / "data" / "spss.sav",
        user_missing=True
    )

    #reserve code check
    ##get unique missing values from field-specific metaobject (ie reserve codes)
    meta_missing = {}
    for vals in meta_output.missing_ranges.values():
        for val in vals:
            meta_missing.update(val)

    #format reserve codes from resource schema
    resource_missing = [int(v) for v in resource.schema.missing_values]
    resource_missing = {'lo':min(resource_missing),'hi':max(resource_missing)}

    # check data written properly
    assert (df_output==resource_df).all().all()
    # check missing values
    assert meta_missing==resource_missing
    #TODO: checks for variable labels (ie descriptions) and value labels etc


if __name__ == "__main__":
    test_spss_parser_write_with_missingvalues()
    print("SUCCESS")
