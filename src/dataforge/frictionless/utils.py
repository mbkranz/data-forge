""" 
utilities and convenience functions

"""

import pandas as pd
from pathlib import Path
import json
from frictionless import validate,transform
from frictionless import Resource,Package

## Package functions
## Resource functions
## Report (validation) functions
def _make_cell_errordf(task):
    if task['errors']:
        df = pd.DataFrame([{'column':c['fieldName'],'value':c['cell'],'error':c['note']}
            for c in task['errors'] if '#cell' in c['tags']])
    else:
        df = pd.DataFrame()
    
    return df.assign(resource=task.resource.name)

def write_package_report(package:Package,outdir="",is_write_to_file=True):
    """ 
    validates package, and creates a few useful outputs
    from the validation Report object. If specified, 
    writes these to a file (default).

    These include:
    - report.txt: The out of the box summary text file you would see, for example, via the command line
    - report.json: A json file from the orignal returned report to use for programmatically verifying if valid and versioning
    - report_condensed_cell_errors.tsv: [Very helpful for curation as cell errors are most common] As a report returns errors for each cell, this condensed table helps to quickly identify
        all errors on a cell basis. One should only look at cell errors after table and row errors are resolved.
    - report_missing_fields.tsv: (need to run the add_missing_fields transform) list each resource's missing values


    Parameters
    ----------------
    package: frictionless package
    """

    summarypath = Path(outdir)/'report.txt'
    reportpath = Path(outdir)/"report.json"
    condensedpath = Path(outdir)/'report_condensed_cell_errors.tsv'
    missingpath = Path(outdir)/'report_missing_fields.tsv'

    # get missing fields added via transform
    missing_added = []
    for r in package.resources:
        resource_missing_added = r.get('custom',{}).get('dataforge:missing_fields_added',{})
        if resource_missing_added:
            missing_added.extend([{'resource_name':r['name'],'missing_field':f} for f in resource_missing_added])

    report = validate(package)

    # TODO: change to petl transform so no pandas dependency?
    errors_df = pd.concat([_make_cell_errordf(task) for task in report['tasks']]).drop_duplicates()
    missing_fields_df = pd.DataFrame(missing_added)

    report_products = {
        summarypath.name:report.to_summary(),
        reportpath.name:report,
        condensedpath.name:errors_df,
        missingpath.name:missing_fields_df
    }

    # write to file if specified
    if is_write_to_file:
        if missing_added:
            missing_fields_df.to_csv(missingpath,sep='\t')

        with open(summarypath,'w') as f:
            f.write(report.to_summary())

        with open(reportpath,'w') as f:
            json.dump(report,f,indent=4)

        if len(errors_df)>0:
            errors_df.to_csv(condensedpath,sep='\t',index=False)

    
    return report_products