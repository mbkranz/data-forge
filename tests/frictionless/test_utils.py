import os
from pathlib import Path
from frictionless import Package
from frictionless import transform
from dataforge.frictionless import write_package_report
from dataforge.frictionless import add_missing_fields
# os.chdir("../../")
# def test_write_package_report():

def test_write_package_report_header_errors():
    package = Package('data/missingfields-package.json')
    write_package_report(package)

    files = ['report_condensed_cell_errors.tsv','report.json','report.txt']
    
    #TODO: assert statements for content
    for f in files:
        assert Path(f).exists()
        os.remove(f)


def test_write_package_report_cell_errors():
    package = Package('data/cell-errors-package.json')
    package_with_missing = transform(package,steps=[add_missing_fields(missing_value='Missing')])
    report_products = write_package_report(package_with_missing,is_write_to_file=False)

    files = ['report_condensed_cell_errors.tsv','report.json','report.txt','report_missing_fields.tsv']
    
    #TODO: tests for content (tested while debugging but ran out of time)
    assert [Path(f).stem for f in files]==list(report_products.keys())    

if __name__=="__main__":
    test_write_package_report_header_errors()
    test_write_package_report_cell_errors()
    print("SUCCESS")