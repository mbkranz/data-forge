import os
from pathlib import Path
import petl as etl
#os.chdir("../../")
from frictionless import Resource,Schema,Package
from frictionless import transform,validate

from dataforge.frictionless.transforms import *


def test_add_missing_fields():
    tbl = etl.fromcsv("data/countries-with-missing/data/input.csv").cutout('name')
    schema = Schema("data/countries-with-missing/schemas/input.json")
    schema.missing_values = ['Missing','Not asked','Refused',"Don't know"]
    source = Resource(data=tbl.todf().to_dict(orient='records'),schema=schema)
    target = transform(source, steps=[add_missing_fields(missing_value='Missing')])


    assert not validate(source)['valid']
    assert validate(target)['valid']
    assert target['custom']['dataforge:missing_fields_added']==['name']


def test_add_missing_fields_to_package():
    # def test_add_resources_missing_fields():
    source = Package("data/missingfields-package.json")
    target = transform(source, steps=[add_missing_fields(missing_value='Missing')])
    
    assert not validate(source)['valid']
    assert validate(target)['valid']
    assert target.resources[0]['custom']['dataforge:missing_fields_added']==['age']
    assert target.resources[1]['custom']['dataforge:missing_fields_added']==['occupation','age']



if __name__=="__main__":
    test_add_missing_fields()
    print("SUCCESS")