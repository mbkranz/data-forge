"""Frictionless plugin providing Stata support

   Module named frictionless_dataforgestata instead of frictionless_stata to
   avoid confusion with possible future official Stata plugin.
"""

from frictionless import Parser, Control, Plugin, system
import pyreadstat
import re
import pandas as pd
import attrs

from collections.abc import MutableMapping,MutableSequence

class StataParser(Parser):
    """Stata parser implementation"""

    supported_types = [
        'string',
    ]

    # Read

    def read_cell_stream_create(self):
        raise NotImplementedError('Reading Stata (.dta) files not yet implemented')

    # Write
    def __delete_dot_notation(self,iterable):
        ''' 
        Delete dot notation of either 
        keys in a mapping (eg dictionary) or 
        sequence (eg list) 
        ''' 

        for key in list(iterable):
            if re.match('^\.[a-zA-Z]$', str(key)):
                new_key = key.lower().replace('.', '')

                if isinstance(iterable, MutableMapping):
                    iterable[new_key] = iterable.pop(key)
                elif isinstance(iterable,MutableSequence):
                    key_index = iterable.index(key)
                    iterable[key_index] = new_key
        
        return iterable

    def __get_value_labels(self, source):
        
        # final maps to populate for readstat
        value_labels = {}
        missing_vals = {}

        # table wide (ie schema lvl) reserve codes and corresponding encodings
        reserve_codes = self.__delete_dot_notation(source.schema.get('missingValues',[]))
        reserve_encodings = self.__delete_dot_notation(source.schema.get('encoding',{}))

        for field in source.schema.fields:
            missing_vals[field['name']] = []
            # add reserve codes to missing values
            if not field.get('required'):
                if reserve_codes:
                    missing_vals[field['name']].extend(reserve_codes)
            
            # TODO: add field wise missing values if present 

            # add encodings (including reserve codes) to value_labels
            encoding = {int(key):val for key,val in field.get('encoding',{}).items()}
            encoding = self.__delete_dot_notation(encoding)
            encoding.update(reserve_encodings)
            
            value_labels[field['name']] = encoding
        
        return value_labels, missing_vals

    def write_row_stream(self, source):
        # NOTE:write_row_stream taken from the dataforgespss write_row_stream with exxception
        # of changing missing_ranges to missing_user_values and not specifying downcast in 
        # coercion to int (see below NOTE)

        # NOTE: for numeric fields (ie no value labels except missing values) with missing values
        ## sing downcast='integer' in call, (conversion to int8): Pyreadstaterror --> "missing_user_values not allowed for character variable"
        ## without downcast, no error (conversion to int64)
        ## could be due to pyreadstat converting int8 to integer and int64 to numeric (see pyreadstat README)
        ## One thing to try may be to explicitly specify types in write.

            
        value_labels, missing_vals = self.__get_value_labels(source)

        prop_for_column_lbls = 'title' #TODO: change to allow either title or description?

        column_labels = {
            f['name']:f.get(prop_for_column_lbls,'') 
            for f in source.schema.fields
        }
        df = source.to_petl().todataframe()

        # TODO: What if this is a number (ie float column)? We wouldn't want to downcast so we may need
            # a where clause based on integers in schema
        df = df.apply(pd.to_numeric, errors='ignore')
        
        # delete missing vals if not type numeric return metadata
        deleted_missing_vals = {}
        for field in list(missing_vals):
            is_numeric = pd.api.types.is_numeric_dtype(
                df[field]
            )
            if not is_numeric:
                deleted_missing_vals[field] = missing_vals.pop(field)


        pyreadstat_params = {
            'column_labels':column_labels,
            'missing_user_values':missing_vals,
            'variable_value_labels':value_labels,
        }        

        pyreadstat.write_dta(df, self.resource.path,**pyreadstat_params)

        return {
            'pyreadstat_params':pyreadstat_params,
            'nonnumeric_variables':list(deleted_missing_vals)
        }

@attrs.define(kw_only=True)
class StataControl(Control):
    """Stata dialect representation"""

    type = 'stata'

class DataforgeStataPlugin(Plugin):
    """Plugin for Stata"""

    def create_parser(self, resource):
        if resource.format == 'dta':
            return StataParser(resource)

    def detect_resource(self, resource):
        if resource.format == 'dta':
            resource.type = 'table'

    def select_Control(self, type):
        if type == 'stata':
            return StataControl

system.register('dataforgestata', DataforgeStataPlugin())
