import shutil
import pandas as pd
from pathlib import Path
import numpy as np
from dataforge import tools
from git import Repo
import git


df = pd.DataFrame(
    {
    'record_id': {0: 1, 1: 1, 2: 2, 3: 2, 4: 3},
    'test_var': {0: 'hello', 1: 'world', 2: 'this', 3: 'is', 4: 'roger'},
    'date_var': {0: '1/1/2023',
    1: '1/1/2023',
    2: '1/1/2023',
    3: '1/1/2023',
    4: '1/1/2023'}
  }
)
offset_file = "offsets.csv"

def test_date_offset_no_version_cntrl():
    """ test the dataforge tools.date_offset without version control """ 

    offsets = tools.date_offset(
        key=df['record_id'],
        offset_file=offset_file,
        name='days_for_shift_date',
        seed=[2])
        
    assert offsets.values.tolist()==[124, -87, -142]

    os.remove(offset_file)

def test_date_offset_with_version_control():
    """test offsets with git bare repo (ie remote url)"""

    # create a git bare repo
    test_remote_url = Path("./offsets.git").resolve().as_posix()
    remoterepo = Repo.init(test_remote_url,bare=True)



    ids = df['record_id'].iloc[:-2]
    offsets = tools.date_offset(
        key=ids,
        offset_file=offset_file,
        offset_url=test_remote_url,
        name='days_for_shift_date',
        seed=[2])

    localrepo = Repo(f"tmp/git/{Path(offset_file).stem}")

    assert len(list(localrepo.iter_commits()))==1
    assert len(list(remoterepo.iter_commits()))==1
    assert offsets.values.tolist()==[124,-87]

    ids2 = df['record_id']
    offsets2 = tools.date_offset(
        key=ids2,
        offset_file=offset_file,
        offset_url=test_remote_url,
        name='days_for_shift_date',
        seed=[2])

    assert offsets2.values.tolist()==[124,-87,-142]
    assert len(list(localrepo.iter_commits()))==2
    assert len(list(remoterepo.iter_commits()))==2

    localrepo.close()
    git.rmtree("tmp/git/offsets")
    remoterepo.close()
    git.rmtree("offsets.git")
    shutil.rmtree("tmp")
