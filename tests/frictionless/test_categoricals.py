import os
from pathlib import Path

# os.chdir("../../")
from frictionless import Package
from frictionless import transform

from dataforge.frictionless.categoricals import table_encode


def test_table_encode_transform_pipeline():
    pass


def test_table_encode_transform_pipeline_with_missing_values():
    encodings = {
        "name": {
            "1": "Britain",
            "2": "France",
            "3": "Germany",
            "4": "Italy",
            "5": "Spain",
            "6": "Oz",
        }
    }

    reserve_codes = {
        "-99": "Missing",
        "-98": "Not asked",
        "-97": "Refused",
        "-96": "Don't know",
    }

    datapackage = Path("data/countries-with-missing/data-package.json").resolve()
    source = Package(datapackage).get_resource("input")
    target = transform(source, steps=[table_encode(encodings, reserve_codes)])
    # # mapped data (eg encodings and reserve codes mapped)
    # NOTE: read_rows sets missing values (-98 and -99 as None...?)
    ## see target.to_petl().todf() vs. target.read_rows()
    #TODO: instead of hardcoding just compare to outputs (spss.csv and spss.json)
    assert target.to_petl().todf().to_dict(orient="records") == [
        {"id": "1", "capital_id": "1", "name": "1", "population": "67"},
        {"id": "2", "capital_id": "3", "name": "2", "population": "67"},
        {"id": "3", "capital_id": "2", "name": "3", "population": "-96"},
        {"id": "4", "capital_id": "5", "name": "4", "population": "-98"},
        {"id": "5", "capital_id": "4", "name": "5", "population": "47"},
        {"id": "7", "capital_id": "5", "name": "-99", "population": "1000"},
        {"id": "6", "capital_id": "6", "name": "6", "population": "-97"},
    ]
    assert target.schema == {
        "fields": [
            {"type": "integer", "name": "id"},
            {"type": "integer", "name": "capital_id"},
            {
                "type": "any",
                "name": "name",
                "constraints": {"enum": ["3", "1", "2", "4", "5", "6"]},
                "encoding": {
                    "1": "Britain",
                    "2": "France",
                    "3": "Germany",
                    "4": "Italy",
                    "5": "Spain",
                    "6": "Oz",
                },
            },
            {"type": "integer", "name": "population"},
        ],
        "missingValues": ["-99", "-98", "-97", "-96"],
        "encoding": {
            "-99": "Missing",
            "-98": "Not asked",
            "-97": "Refused",
            "-96": "Don't know",
        },
    }




if __name__=="__main__":
    test_table_encode_transform_pipeline_with_missing_values()
    print("SUCCESS")
