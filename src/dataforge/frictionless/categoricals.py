"""
Functions for managing categorical fields

TODO: Would it be good to turn this into a Frictionless Step Plugin class to follow template?
https://v4.framework.frictionlessdata.io/docs/guides/extension/step-guide
TODO: infer types after encoding replacements
"""

def table_encode(encodings:dict[dict],reservecodes:dict=None,in_data='values'):
    """Encode fields in table
    
    For the moment we support only extended missing values specified in the
    encodings.
    """
    assert in_data in ['keys','values']

    def _flip_if(encoding):
        if in_data=='values':
            return {v:k for k,v in encoding.items()}
        elif in_data=='keys':
            return encoding

    def _select_if(iterable,mapping):
        return [mapping.get(val,val) for val in iterable]
    
    def step(resource):
        fields = [field['name'] for field in resource.schema.fields]
        reserve_mapping = _flip_if(reservecodes) if reservecodes else {}
        field_mappings = {var:_flip_if(encodings[var]) for var in encodings
            if var in fields}
        current = resource.to_copy()
        encoded = [index for index, field in enumerate(current.schema['fields'])
                   if field['name'] in encodings]
        
        # Data
        def data():
            table = current.to_petl()
            return (
                table
                .convertall(reserve_mapping)
                .convert(field_mappings)
            )
        
        # Meta
        resource.data = data

        # replace individual field values in schema 
        for fieldname,mapping in field_mappings.items():
            field = resource.schema.get_field(fieldname)
            field['type'] = 'any'
            field['encoding'] = _flip_if(field_mappings[fieldname])
            if 'missingValues' in field:
                field['missingValues'] = [
                    mapping.get(val,val) 
                    for val in field['missingValues']
                ]

            if field.get('constraints',{}).get('enum',{}):
                field['constraints']['enum'] = [
                    mapping.get(val,val) 
                    for val in field['constraints']['enum']
                ]

            #TODO: other metadata fields to replace?
        
        # replace resource wide values
        if 'missingValues' in resource.schema:
            resource.schema['missingValues'] = [
                reserve_mapping.get(val,val) 
                for val in resource.schema['missingValues']
            ]

        resource.schema['encoding'] = _flip_if(reserve_mapping) 
        
    return step
