"""Frictionless plugin providing SPSS support

   Module named frictionless_dataforgespss instead of frictionless_spss to
   avoid confusion with current SPSS plugin that SavReader module rather than
   pyreadstat.

TODO: check that it the missing values are consecutive integers
TODO: check if key is numeric (necessary for SPSS for missing range encoded values) 
TODO: (see todo in code) add field wise missing values
TODO: add ordered (ie ordinal property) and other SPSS measure specs
"""

from frictionless import Parser, Control, Plugin, system
import pyreadstat
import re
import pandas as pd
import attrs

class SPSSParser(Parser):
    """SPSS parser implementation"""

    supported_types = [
        'string',
    ]

    # Read

    def read_cell_stream_create(self):
        raise NotImplementedError('Reading SPSS (.sav) files not yet implemented')

    # Write

    def __get_value_labels(self, source):
        
        # final maps to populate for readstat
        value_labels = {}
        missing_vals = {}

        # table wide (ie schema lvl) reserve codes and corresponding encodings
        reserve_codes = [int(key) for key in source.schema.get('missingValues',[])]
        reserve_encodings = {int(key):val for key,val in source.schema.get('encoding',{}).items()}

        for field in source.schema.fields:
            # add reserve codes to missing values
            not_required = not field.get('constraints',{}).get('required')
            if not_required:
                missing_vals[field['name']] = []
                if reserve_codes:
                    missing_vals[field['name']].append(
                        {'lo':min(reserve_codes),'hi':max(reserve_codes)}
                    )
            
            # TODO: add field wise missing values if present

            # add encodings (including reserve codes) to value_labels
            encoding = {int(key):val for key,val in field.get('encoding',{}).items()}
            encoding.update(reserve_encodings)
            value_labels[field['name']] = encoding
        
        return value_labels, missing_vals

    def write_row_stream(self, source):
        
        value_labels, missing_vals = self.__get_value_labels(source)

        prop_for_column_lbls = 'title' #TODO: change to allow either title or description?

        column_labels = {
            f['name']:f.get(prop_for_column_lbls,'') 
            for f in source.schema.fields
        }
        df = source.to_petl().todataframe()

        # TODO: What if this is a number (ie float column)? We wouldn't want to downcast so we may need
            # a where clause based on integers in schema
        df = df.apply(pd.to_numeric, downcast='integer', errors='ignore')
        
        # delete missing vals if not type numeric return metadata
        deleted_missing_vals = {}
        for field in list(missing_vals):
            is_numeric = pd.api.types.is_numeric_dtype(
                df[field]
            )
            if not is_numeric:
                deleted_missing_vals[field] = missing_vals.pop(field)


        pyreadstat_params = {
            'column_labels':column_labels,
            'missing_ranges':missing_vals,
            'variable_value_labels':value_labels,
        }        

        pyreadstat.write_sav(df, self.resource.path,**pyreadstat_params)

        return {
            'pyreadstat_params':pyreadstat_params,
            'nonnumeric_variables':list(deleted_missing_vals)
        }
@attrs.define(kw_only=True)
class SPSSControl(Control):
    """SPSS dialect representation"""

    type = 'spss'

class DataforgeSPSSPlugin(Plugin):
    """Plugin for SPSS"""

    def create_parser(self, resource):
        if resource.format == 'sav':
            return SPSSParser(resource)

    def detect_resource(self, resource):
        if resource.format == 'sav':
            resource.type = 'table'

    def select_Control(self, type):
        if type == 'spss':
            return SPSSControl

system.register('dataforgespss', DataforgeSPSSPlugin())
