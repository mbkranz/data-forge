"""
frictionless transform steps
"""
from frictionless import Step
from frictionless import transform,steps

class add_missing_fields(Step):
    """ 
    This transform step adds missing fields that are in the schema
    but not in the data. It then adds a custom property called
    custom.dataforge:added_fields to the resource metadata
    
    The missing value to fill empty cells in added fields
    NOTE: MUST be one of the missing_values within
    the schema of the resource. If transforming a package, 
    currently only supports one missing value across resources.

    """

    type = "add-missing-fields"

    def __init__(self,descriptor=None,missing_value:str=None):
        self.setinitial('missing_value',missing_value)
        super().__init__(descriptor)

    def transform_resource(self, resource):

        missing_value = self.get('missing_value')
        tbl = resource.to_petl()

        fieldnames = tbl.fieldnames()
        fields = []
        fields_to_add = []
        for field in resource.schema.fields:
            fields.append(field.name)
            if field.name not in fieldnames:
                fields_to_add.append((field.name, missing_value))

        resource.data = (
            tbl
            .addfields(fields_to_add)
            .cut(fields)
        )

        custom = resource.get('custom',{})
        custom['dataforge:missing_fields_added'] = list(dict(fields_to_add).keys())
        resource['custom'] = custom
        return resource

    def transform_package(self,package):
        """ 
        applies add_missing_fields to all resources in a package
        """

        missing_value = self.get('missing_value')
        target_resources = [
                transform(r,steps=[add_missing_fields(missing_value=missing_value)])
                for r in package.resources
            ]
        package.resources = target_resources    
        return package 
    # Metadata

    metadata_profile_patch = {
        "required": ["missingValue"],
        "properties": {
            "missingValue": {"type": "string"}
        },
    }


class encode_table(Step):
    """ 
    Encodes a table (ie adds value labels etc and converts missing values) into given 
    mappings. 
    """

    type = "encode-table"

    def __init__(self,descriptor=None,encodings:dict[dict]=None,reservecodes:dict=None,in_data='values'):

        self.setinitial('encodings',encodings)
        self.setinitial('reservecodes',reservecodes)
        self.setinitial('in_data',in_data)

        assert in_data in ['keys','values']

        super().__init__(descriptor)

    def transform_resource(self, resource):

        def _flip_if(encoding):
            if in_data=='values':
                return {v:k for k,v in encoding.items()}
            elif in_data=='keys':
                return encoding

        def _select_if(iterable,mapping):
            return [mapping.get(val,val) for val in iterable]

        encodings = self.get('encodings')
        reservecodes = self.get('reservecodes')
        in_data = self.get('in_data')

        fields = [field['name'] for field in resource.schema.fields]
        reserve_mapping = _flip_if(reservecodes) if reservecodes else {}
        field_mappings = {var:_flip_if(encodings[var]) for var in encodings
            if var in fields}
        encoded = [index for index, field in enumerate(resource.schema['fields'])
                   if field['name'] in encodings]

        # replace individual field values in schema 
        for fieldname,mapping in field_mappings.items():
            field = resource.schema.get_field(fieldname)
            field['type'] = 'any'
            field['encoding'] = _flip_if(field_mappings[fieldname])
            if 'missingValues' in field:
                field['missingValues'] = [
                    mapping.get(val,val) 
                    for val in field['missingValues']
                ]

            if field.get('constraints',{}).get('enum',{}):
                field['constraints']['enum'] = [
                    mapping.get(val,val) 
                    for val in field['constraints']['enum']
                ]

            #TODO: other metadata fields to replace?
        
        # replace resource wide values
        if 'missingValues' in resource.schema and reserve_mapping:

            resource.schema['missingValues'] = [
                reserve_mapping.get(val,val) 
                for val in resource.schema['missingValues']
            ]
        resource.schema['encoding'] = _flip_if(reserve_mapping) 

        # replace values in data
        table = resource.to_petl()
        resource.data = (
                table
                .convertall(reserve_mapping)
                .convert(field_mappings)
            )

        return resource

    # Metadata

    metadata_profile_patch = {
        "required": ["missingValue"],
        "properties": {
            "missingValue": {"type": "string"}
        },
    }