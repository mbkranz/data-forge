from dataforge.frictionless.categoricals import table_encode
from frictionless import transform
from frictionless import Package
from pathlib import Path
import pyreadstat
import pandas as pd

def test_stata_parser_write_with_missingvalues():

    datapackage = Path("data/countries-with-missing/data-package.json").resolve()
    resource = Package(datapackage).get_resource("stata-csv")
    resource.write(datapackage.parent / "data" / "stata.dta")

    #apply types but not missing values (TODO: probably a way to do via frictionless)
    resource_df = resource.to_petl().todf().applymap(pd.to_numeric,errors='ignore')

    # read in the newly written sav file 
    df_output, meta_output = pyreadstat.read_dta(
        datapackage.parent / "data" / "stata.dta",
        user_missing=True
    )

    #reserve code check
    ##get unique missing values from field-specific metaobject (ie reserve codes)
    meta_missing = []
    for vals in meta_output.missing_user_values.values():
        for val in vals:
            meta_missing.append(val)

    #format reserve codes from resource schema
    resource_missing = resource.schema.missing_values

    # check data written properly
    assert (df_output==resource_df).all().all()
    # check missing values
    assert sorted(meta_missing)==sorted(resource_missing)
    #TODO: checks for variable labels (ie descriptions) and value labels etc


if __name__ == "__main__":
    test_stata_parser_write_with_missingvalues()
    print("SUCCESS")
